const TicketController = require("../controllers/ticketController");
//require('should');
const Ticket = require('../models/Ticket')
const User = require('../models/User')

process.env.ENV = 'Test';
process.env.MONGODB = "mongodb://localhost:27017/bus-ticket_Test"
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../app");
let should = chai.should();
chai.use(chaiHttp);
// const login = 
//     {
//         "username": "admin",
//         "password": "password"
//     };
const testBooking = {
    "seat_number": 3,
    "passenger": { 
        "name": "John Doe",
        "sex": "M",
        "age": 65,
        "phone": "918093689",
        "email": "1a32123ad@gmail.com"
    }
}
// const updateBooking = {
//     "is_booked": false, 
//     "passenger": {
//         "name": "Jahangir Doe",
//         "sex": "F",
//         "age": 85,
//         "phone": "9565986584",
//         "email": "mumtaz@gmail.com"
//     }
// }
// const checkID="5e8c1338ca8c333d38290c63";
describe('Bus Ticket Controller Tests:', ()=>{
    before((done) => { 
        Ticket.deleteMany({}).exec();
        User.deleteMany({}).exec();
        done();     
	});
    describe('/Post Create Booking',()=>{
        it('should allow adding valid ticket with details', (done)=>{
            chai.request(server)
           .post("/api/ticket")
           .send(testBooking)
           .end((err,res)=>{
 //              console.log(res.body);
               res.should.have.status(200);
//               res.should.have.property("text").eql('"Already exist"');
               done();
           }) 
        });
    });
//     describe('/Put Update Ticket',()=>{
//         it('should allow change for valid ticket_id', (done)=>{
//             chai.request(server)
//            .put("/api/ticket/" + checkID)
//            .send(updateBooking)
//            .end((err,res)=>{
//   //            console.log(res.body);
//                res.should.have.status(200);
//                done();
//            }) 
//         });
//     });
   
    describe('/Get Closed',()=>{
        it('should fetch all closed tickets', (done)=>{
           //console.log("Heyyyyyyy")
            chai.request(server)
           .get("/api/tickets/closed")
           .end((err,res)=>{
               res.should.have.status(200);
               done();
           }) 
        });
    });
    describe('/Get Open Tickets',()=>{
        it('should fetch for open tickets', (done)=>{
            chai.request(server)
           .get("/api/tickets/open")
           .end((err,res)=>{
//               console.log(res.body);
               res.should.have.status(200);
               done();
           }) 
        });
    });
//     describe('/Put Update User Details',()=>{
//         it('should allow change for valid ticket_id', (done)=>{
//             chai.request(server)
//            .put("/api/user/"+checkID)
//            .send(updateBooking)
//            .end((err,res)=>{
//   //             console.log(res.body);
//                res.should.have.status(200);
//                done();
//            }) 
//         });
//     });
    // describe('/Get User Details',()=>{
    //     it('should fetch user details for valid ticket_id', (done)=>{
    //         chai.request(server)
    //        .get("/api/ticket/details/"+checkID)
    //        .end((err,res)=>{
    //  //          console.log(res.body);
    //            res.should.have.status(200);
    //            done();
    //        }) 
    //     });
    // });
    // describe('/Reset The Database',()=>{
    //     it('should reset full database', (done)=>{
    //         chai.request(server)
    //        .post("/api/tickets/reset")
    //        .send(login)
    //        .end((err,res)=>{
    // //           console.log(res);
    //            res.should.have.status(200);
    //            done();
    //        }) 
    //     });
    // });
    after((done) => { 
        Ticket.deleteMany({}).exec();
        User.deleteMany({}).exec();
        done();     
	});
});