const express = require('express')
const Ticket = require('../models/Ticket')
const User = require('../models/User')
const validation = require('../middleware/validation/validation')
const bcrypt = require('bcrypt')
const userValidation = validation.userValidation
const openTicket = validation.openTicket
const TicketController = require("../controllers/ticketController");
var cookieParser = require('cookie-parser');
const router = express.Router()

router

//start new bus services
router.post('/start', TicketController.startNew);

//create a ticket
router.post('/ticket', TicketController.bookTicket);

//update a ticket, update open/closed and user_details
router.put('/ticket/:ticket_id', TicketController.updateTicket);

//edit details of user
router.put('/user/:ticket_id', TicketController.updateUser);

// get the status of a ticket based on ticket_id
router.get('/ticket/:ticket_id', TicketController.ticketStatus);

// get list of all open tickets
router.get('/tickets/open', TicketController.openTickets);

// get list of all closed tickets
router.get('/tickets/closed', TicketController.closedTickets);

// View person details of a ticket
router.get('/ticket/details/:ticket_id', TicketController.userDetails);

//Reset
router.post('/tickets/reset', TicketController.resets);

module.exports = router;
