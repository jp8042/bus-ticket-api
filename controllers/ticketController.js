const express = require('express')
const Ticket = require('../models/Ticket')
const User = require('../models/User')
const validation = require('../middleware/validation/validation')
const bcrypt = require('bcrypt')
const userValidation = validation.userValidation
const openTicket = validation.openTicket
var cookieParser = require('cookie-parser');
const router = express.Router();
//var apiResponse = require("./helpers/apiResponse");

exports.bookTicket = function(req, res){
    var flag = true;
    Ticket.findOne({seat_number : req.body.seat_number}, function(err, ticket){
//     console.log(ticket!==null && ticket.is_booked!==false);

		//Check if ticket exist with true is_booked
        if (ticket!==null && ticket.is_booked!==false) {

            return res.status(400).json("Already exist. To Change Update Your Ticket");
   
        }
		//Else Create a new ticket, user already exists or create a new user aswell
        else
        {
            const passengerDetails = req.body.passenger; 
            User.findOne({email:passengerDetails.email}, function(err, userDetail){
//				console.log(userDetail);
				//None data exist with this email id, create a new user
                if(userDetail===null)
                {
                    let [result, data] = userValidation(req.body.passenger)
//                    console.log(result);
                    if (!result) return res.status(404).json({ message: data })
                    const user = new User(req.body.passenger)
                
                    const ticket = new Ticket({ seat_number: req.body.seat_number })
                    user.save(function(err, data){
//							console.log(data);
							if (data)  
                            {

                                ticket.passenger = user._id
                                Ticket.findOneAndRemove({ seat_number: req.body.seat_number, is_booked : false }, function(err, newticket)
                                {
//									console.log(newticket);
//                                    if(err) return res.status(400).json({message:err});
									
                                    ticket.save()
                                        .then(data => res.status(200).json(data))
                                        .catch(err => {
                                        err => res.status(400).json({ message: err });
                                        })
                                    
                                });
							}
						})    
                }
				//User Already Exist here,  Create a new ticket
                else
                {
					Ticket.findOne({passenger:userDetail._id}, function(err, repeatTicket){
						if(repeatTicket===null)
						{
							const ticket = new Ticket({ seat_number: req.body.seat_number })
							ticket.passenger = userDetail._id;
							Ticket.findOne({ seat_number: req.body.seat_number }, function(err, ticket)
							{
								if(err) return res.status(400).json({message:err});
								if(ticket.is_booked === false)
								{
									ticket.is_booked = true;
									ticket.passenger = userDetail._id;
									ticket.save()
									.then(data => res.status(200).json(data))
									.catch(err => {
									err => res.status(400).json({ message: err });
									})
								}
								else
								{
									return res.status(400).json({message:"Already Booked"});
								}
							});
						}
						if(repeatTicket !== null)
						{
							return res.status(400).json({message:"1 user 1 ticket"});
						}
					});
                }
            })
                        
        }
    });
   
}

exports.updateTicket = function(req, res) {
    const { ticket_id } = req.params
    const payload = req.body
    let passenger = null

    if ('passenger' in payload) {
        passenger = req.body.passenger
    }

    if (payload.is_booked === true && passenger !== null) {
        Ticket.findById(ticket_id, function (err, ticket) {
            if(ticket !== null)
			{
				User.findOne({email:passenger.email}, function(err, userDetail){
					console.log(userDetail);
					if(userDetail===null)
					{
						let [result, data] = userValidation(passenger)
                    console.log(result);
                    if (!result) return res.status(404).json({ message: data })
                    const user = new User(passenger)
                
//                    const ticket = new Ticket({ seat_number: req.body.seat_number })
                    user.save(function(err, data){
							console.log(data);
							if (data)  
                            {
                                ticket.passenger  = data._id
								ticket.save(function(err,data)
								{
									if(data)res.status(200).json(data);
									else res.status(400).json({message:err});
								});
									
							}
						})
					}    
			//User Already Exist here,  Create a new ticket
				else
				{
					Ticket.findOne({passenger:userDetail._id}, function(err, repeatTicket){
					if(repeatTicket===null)
					{
//					const ticket = new Ticket({ seat_number: req.body.seat_number })
						ticket.passenger = userDetail._id;
						Ticket.findOne({ seat_number: req.body.seat_number }, function(err, ticket)
						{
							if(err) return res.status(400).json({message:err});
							if(ticket.is_booked === false)
							{
								ticket.is_booked = true;
								ticket.passenger = userDetail._id;
								ticket.save()
								.then(data => res.status(200).json(data))
								.catch(err => {
									err => res.status(400).json({ message: err });
								})
							}
							else
							{
								return res.status(400).json({message:"Already Booked"});
							}
						});
					}	
			
					if(repeatTicket !== null)
					{
						return res.status(400).json({message:"1 user 1 ticket"});
					}
					});
				}
			})
			}
		});
	}
	else if(payload.is_booked === false)
    {
        Ticket.findById(ticket_id, function(err, ticket)
        {
            if(err) res.status(404).json({ message: err });
            if(ticket)
            {
                ticket.is_booked = false;
                ticket.passenger=null;
            }
        })
    }
    else
    {
        return res.status(400).json({message:"Passenger Details Required."});
    }
}

exports.updateUser = function(req, res)  {
    const { ticket_id } = req.params
    const payload = req.body

    Ticket.findById(ticket_id, function (err, ticket) {
        if (err) res.status(404).json({ message: err })
        if (ticket) {
            const user_id = ticket.passenger
            User.findById(user_id)
                .then(user => {
                    if ('name' in payload) user.name = payload.name
                    if ('sex' in payload) user.sex = payload.sex
                    if ('email' in payload) user.email = payload.email
                    if ('phone' in payload) user.phone = payload.phone
                    if ('age' in payload) user.age = payload.age
                    user.save(function(err, data){
						console.log(data);
						if(data) return res.status(200).json(data)
					})
                })
                .catch(err => res.status(404).json({ message: err }))
        }
    })
}

exports.ticketStatus = function(req, res) {
    const { ticket_id } = req.params
    Ticket.findById(ticket_id, function (err, ticket) {
        if (err) res.status(404).json({ message: err })
        if (ticket) res.status(200).json({ status: ticket.is_booked })
    })
}

exports.openTickets = function(req, res) {
    Ticket.find({ is_booked: false }, (err, data) => {
        if (err) res.status(404).json({ message: err })
//			console.log(data);
        if (data!==null) res.status(200).json(data);
    })
}

exports.closedTickets = function(req, res) {
    Ticket.find({ is_booked: true }, (err, data) => {
        if (err) res.status(404).json({ message: err })
        if (data) res.status(200).json(data)
    })
}

exports.userDetails = function(req, res) {
    const { ticket_id } = req.params
    return Ticket.findById(ticket_id, function(err, ticket) {
 //       console.log(ticket.passenger);
        if (err) res.status(400).json({ message: err })
        if (ticket) {
//            console.log(err);
            User.findById(ticket.passenger, function(err, user) {
//               console.log(user);
                if (err !== null)  res.status(404).json({ message: err })
                if (user) res.status(200).json(user)

            })
        }
    })
}

exports.resets = function(req, res) {

    if (!("username" in req.body) && !("password" in req.body)) {
        res.status(400).json({ message: "username and password is needed in request body" })
    }

    const { username, password } = req.body

    if (!(password === "password") || !(username === "admin")) {
        res.status(400).json({ message: "username or password is incorrect" })
    }

    Ticket.find({is_booked:true}, (err, data) => {
        if (err) res.status(404).json({ message: err })
        if (data) {
            data.forEach(openTicket)
            res.status(200).json({ message: "success" })
        }
    })

}

exports.startNew = function(req, res){
    if (!("username" in req.body) && !("password" in req.body)) {
        res.status(400).json({ message: "username and password is needed in request body" })
    }

    const { username, password } = req.body

    if (!(password === "password") || !(username === "admin")) {
        res.status(400).json({ message: "username or password is incorrect" })
    }
    var ticket_number = 1;
    // Ticket.deleteMany({});
    // User.deleteMany({});
    for(;ticket_number<=40;ticket_number++)
    {
        const ticket = new Ticket({ seat_number: ticket_number })
        ticket.is_booked = false;
        ticket.passenger = null;

        Ticket.deleteMany({ seat_number: ticket_number }, function(err){
 //           console.log(ticket_number);
            User.remove({}, function(err){
//                console.log("help");
            });
            ticket.save();
        });
        
        
    }
    if(ticket_number<=40)
    {
        return res.status(400).json({message:"Unable to Intialize. Retry the service"});
    }   
    else
        return res.status(200).json({message:"Succesfully Initalized Services"});
}

