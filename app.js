const express = require('express')
const dbConfig = require('./config/database.config.js')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const apiRoutes = require('./routes/ticket')
const cookieParser = require('cookie-parser');
const path = require('path');

//var apiResponse = require("./helpers/apiResponse");

require('dotenv/config')

const app = express()


app.use(cookieParser());
app.use(express.static('public'));
app.use(express.static(path.join(__dirname, 'public')));




app.use(bodyParser.json())//support json encoded bodies
app.use(bodyParser.urlencoded({
		extended: true
}));

app.use((req, res, next) => {
		res.header("Access-Control-Allow-Origin", "*");
		res.header(
				"Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, Authorization"
		);
		if (req.method === "OPTIONS") {
				res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
				return res.status(200).json({});
		}
		next();
});

app.use('/api', apiRoutes)


mongoose.Promise = global.Promise;
if(process.env.ENV==='Test'){
    console.log('This is test!');
    mongoose.connect(process.env.MONGODB,
        { useNewUrlParser: true, useUnifiedTopology: true });
}
else{
mongoose.connect(dbConfig.url
    ,
    { useNewUrlParser: true, useUnifiedTopology: true }
)}

mongoose.connection
    .once('open', () => console.log('Connected to the DB successfully'))
    .on('error', (error) => {
        console.log('Warning');
    });

var PORT  = process.env.PORT || 3000; 
app.listen(PORT, function() {
		console.log('server running at port' + PORT);
})

module.exports = app;